from graphics import *


class Grid:
    def __init__(self, layout):
        self.win = GraphWin('Floor', 500, 500)

        self.win.setCoords(0.0, 0.0, len(layout), len(layout[0]))
        self.win.setBackground("black")
        self.city_grid = []
        self.emergencies = []

        # draw grid
        for x in range(len(layout)):
            for y in range(len(layout[0])):
                self.win.plotPixel(x * 50, y * 50, "blue")

        for i, row in enumerate(layout):
            self.grid_row = []
            for j, elem in enumerate(row):
                if elem.structure == "road":
                    self.draw_road(i, j, self.win, elem.condition)
                elif elem.structure == "building":
                    self.draw_building(i, j, self.win)
                else:
                    self.draw_hospital(i, j, self.win)
            self.city_grid.append(self.grid_row)
        self.win.getMouse()

    def update_map(self, layout):
        for i, row in enumerate(layout):
            for j, elem in enumerate(row):
                if elem.structure == "road":
                    self.update_road(elem.condition, self.city_grid[i][j])

    def draw_ambulance(self, x, y):
        circle = Circle(Point(x + 0.5, y + 0.5), 0.5)
        circle.draw(self.win)
        circle.setFill("white")
        return circle

    def move_ambulance(self, circle, x, y):
        circle.move(x,y)
        #circle.setFill("white")

    def update_ambulance(self, circle, state):
        if state == "arrived" or state == "in transport":
            circle.setFill("grey")
        else:
            circle.setFill("white")

    def draw_emergency(self, x, y):
        text = Text(Point(x + 0.5, y + 0.5), "!!!")
        text.setSize(27)
        text.draw(self.win)
        text.setFill("blue")
        self.emergencies.append(text)

    def update_emergency(self, index, status):
        if status == "active":
            self.emergencies[index].setFill("blue")
        elif status == "arrived":
            self.emergencies[index].undraw()

    def draw_road(self, x, y, window, condition):
        square = Rectangle(Point(x, y), Point(x+1, y+1))
        square.draw(window)
        if condition == "H":
            square.setFill("red")
        elif condition == "M":
            square.setFill("yellow")
        else:
            square.setFill("green")
        self.grid_row.append(square)

    def update_road(self, condition, square):
        if condition == "H":
            square.setFill("red")
        elif condition == "M":
            square.setFill("yellow")
        else:
            square.setFill("green")

    def draw_building(self, x, y, window):
        square = Rectangle(Point(x, y), Point(x + 1, y + 1))
        square.draw(window)
        square.setFill("black")
        self.grid_row.append(square)

    def draw_hospital(self, x, y, window):
        square = Rectangle(Point(x, y), Point(x + 1, y + 1))
        square.draw(window)
        square.setFill("white")
        text = Text(Point(x + 0.5, y + 0.5), "H")
        text.setSize(27)
        text.draw(window)
        text.setFill("black")
        self.grid_row.append(text)
