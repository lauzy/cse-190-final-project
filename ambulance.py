from astar import *
from read_config import read_config
import time


class Ambulance:
    def __init__(self, location, goal, state):
        self.config = read_config()
        self.pos_moves = self.config["possible_moves"]
        self.size = self.config["map_size"]
        self.location = location
        self.state = state
        self.assigned_emergency = None
        self.traffic_wait = 1
        self.goal = goal
        self.move_list = []
        self.drawing = None

    def update_path(self, location, goal, layout):
        self.move_list = find_path(location, goal, layout, self.pos_moves, self.size)[0]

    def move(self, window):
        time.sleep(0.5)
        if self.traffic_wait <= 0:
            move = self.move_list.pop(0)
            self.location = [self.location[0] + move[0], self.location[1] + move[1]]
            window.move_ambulance(self.drawing, move[0], move[1])
            return True
        else:
            self.traffic_wait -= 1
            return False