from city import *
import random
from grid import *


class EmergencyOp:
    def __init__(self):
        """
        Set up the city and draw it
        """
        self.config = read_config()
        random.seed(self.config["seed"])
        self.size = self.config["map_size"]
        self.moves = self.config["possible_moves"]
        self.city = City().layout
        self.hospitals = self.config["hospital_locations"]
        self.animation = Grid(self.city)
        self.emergencies = []
        self.emergency_locs = self.config["emergency_locations"]
        self.simulate()

    def simulate(self):
        iteration = 0
        unassigned_emergencies = 0
        num_active = 0
        # Loop while there are still emergencies yet to happen or there are still unresolved emergencies
        while len(self.emergency_locs) > 0 or num_active > 0:
            traffic_cond_changed = False

            # A new emergency appears on the map
            if iteration % self.config["iterations_between_emergencies"] == 0 and len(self.emergency_locs) > 0:
                self.report_emergency()
                traffic_cond_changed = True
                self.traffic_update(self.emergencies[-1])

                # If hospitals are out of ambulances to deploy, increase number of unassigned emergencies
                if not self.deploy_ambulance(self.emergencies[-1]):
                    unassigned_emergencies += 1
                    self.emergencies[-1].status = "unassigned"

            # Loop through and move ambulances
            for hospital_loc in self.hospitals:
                hospital = self.city[hospital_loc[0]][hospital_loc[1]]
                for ambulance in hospital.ambulances:
                    if not ambulance.state == "idle":

                        # If ambulance moved to a new grid, adjust its traffic counter
                        if ambulance.move(self.animation):
                            ambulance.traffic_wait = self.city[ambulance.location[0]][ambulance.location[1]].delay

                        # If ambulance reached its goal, change its state depending on if it were on the way to an
                        # emergency or transporting someone to safety
                        if ambulance.location == ambulance.goal:
                            if ambulance.state == "en_route":
                                self.neareast_hospital(ambulance)
                                ambulance.assigned_emergency.status = "arrived"
                                self.animation.update_ambulance(ambulance.drawing, ambulance.assigned_emergency.status)

                            # If safely transported someone to hospital, change emergency status to resolved and
                            # go to new emergency location (if any)
                            else:
                                ambulance.assigned_emergency.status = "resolved"
                                ambulance.state = "idle"
                                self.animation.update_ambulance(ambulance.drawing, ambulance.assigned_emergency.status)
                                if unassigned_emergencies > 0:
                                    unassigned_emergencies -= 1
                                    for i, emergency in enumerate(self.emergencies):
                                        if emergency.status == "unassigned":
                                            self.deploy_ambulance(self.emergencies[i])
                                            self.emergencies[i].status = "active"
                                            break

            # Adjust traffic decay
            for row in self.city:
                for element in row:
                    if element.structure == "road":
                        if element.decay_condition():
                            traffic_cond_changed = True

            # Remove emergencies if resolved and undraw them if ambulance has arrived
            num_active = len(self.emergencies)
            for i, emergency in enumerate(self.emergencies):
                if emergency.status == "resolved":
                    num_active -= 1
                if emergency.status == "arrived":
                    self.animation.update_emergency(i, emergency.status)
                    emergency.status = "in transport"

            # If traffic condition changed, update routes
            if traffic_cond_changed:
                for hospital_loc in self.hospitals:
                    hospital = self.city[hospital_loc[0]][hospital_loc[1]]
                    for ambulance in hospital.ambulances:
                        if ambulance.state == "en_route":
                            ambulance.update_path(ambulance.location, ambulance.goal, self.city)
                        elif ambulance.state == "transport":
                            self.neareast_hospital(ambulance)
            if traffic_cond_changed:
                self.animation.update_map(self.city)
                for i, emergency in enumerate(self.emergencies):
                    if emergency.status == "active":
                        self.animation.update_emergency(i, emergency.status)
            iteration += 1
        print "Number of iterations it took to complete: %d" % iteration

    def deploy_ambulance(self, emergency):
        """
        Deploy an ambulance to an emergency and change the ambulance's fields accordingly
        """
        min_cost = float('inf')
        for hospital_loc in self.hospitals:
            hospital = self.city[hospital_loc[0]][hospital_loc[1]]
            for ambulance in hospital.ambulances:
                if ambulance.state == "idle":
                    path, cost = find_path(ambulance.location, emergency.location, self.city, self.moves, self.size)
                    if cost < min_cost:
                        min_cost = cost
                        opt_path = path
                        assigned_amb = ambulance
        if min_cost == float('inf'):
            return False
        else:
            assigned_amb.move_list = opt_path
            assigned_amb.state = "en_route"
            assigned_amb.goal = emergency.location
            assigned_amb.assigned_emergency = emergency
            if assigned_amb.drawing is None:
                assigned_amb.drawing = self.animation.draw_ambulance(assigned_amb.location[0], assigned_amb.location[1])
            else:
                self.animation.update_ambulance(assigned_amb.drawing, assigned_amb.assigned_emergency.status)
            return True

    def neareast_hospital(self, ambulance):
        """
        Finds the nearest hospital for an ambulance and adjusts its goal and path accordingly
        """
        min_cost = float('inf')
        for hospital in self.hospitals:
            path, cost = find_path(ambulance.location, hospital, self.city, self.moves, self.size)
            if cost < min_cost:
                min_cost = cost
                opt_path = path
                hosp_loc = hospital
        ambulance.move_list = opt_path
        ambulance.state = "transport"
        ambulance.goal = hosp_loc

    def report_emergency(self):
        """
        Gives a new emergency
        """
        emergency_loc = self.emergency_locs.pop(0)
        emergency = Emergency(emergency_loc, self.emergency_type(emergency_loc), "active")
        self.animation.draw_emergency(emergency_loc[0], emergency_loc[1])
        self.emergencies.append(emergency)

    def emergency_type(self, location):
        if self.city[location[0]][location[1]] == "-":
            return "building"
        else:
            return "crash"

    def traffic_update(self, emergency):
        """
        Updates traffic according to the new emergency location
        """
        if emergency.type == "building":
            for move in self.moves:
                loc = self.city[emergency.location[0] + move[0]][emergency.location[1] + move[1]]
                if loc.structure == "road":
                    loc.emergency_traffic_update()
        else:
            for move in self.moves:
                loc = [emergency.location[0], emergency.location[1]]
                grid = self.city[loc[0]][loc[1]]
                while grid.structure == "road":
                    grid.emergency_traffic_update()
                    loc = [loc[0] + move[0], loc[1] + move[1]]
                    if not is_wall(loc, self.size, self.city):
                        grid = self.city[loc[0]][loc[1]]
                    else:
                        break


class Emergency:
    def __init__(self, location, type, status):
        self.location = location
        self.type = type
        self.status = status

if __name__ == "__main__":
    operator = EmergencyOp()