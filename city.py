from ambulance import *


class City:
    def __init__(self):
        self.config = read_config()
        self.layout = self.create_city_layout(self.config["traffic_map"])
        self.h_cost = self.config["heavy_traffic_cost"]
        self.m_cost = self.config["medium_traffic_cost"]
        self.c_cost = self.config["medium_traffic_cost"]

    def create_city_layout(self, layout):
        city = []
        for i, row in enumerate(layout):
            city_row = []
            for j, element in enumerate(row):
                if element == "*":
                    city_row.append(Hospital([i,j]))
                elif element == "-":
                    city_row.append(Building())
                else:
                    city_row.append(Road(element))
            city.append(city_row)
        return city


class Road:
    def __init__(self, condition):
        self.config = read_config()
        self.condition = condition
        self.structure = "road"
        if self.condition == "H":
            self.traffic_decay_count = self.config["heavy_traffic_decay_count"]
            self.delay = self.config["heavy_traffic_delay"]
            self.cost = self.config["heavy_traffic_cost"]
        elif self.condition == "M":
            self.traffic_decay_count = self.config["medium_traffic_decay_count"]
            self.delay = self.config["medium_traffic_delay"]
            self.cost = self.config["medium_traffic_cost"]
        else:
            self.traffic_decay_count = self.config["clear_traffic_decay_count"]
            self.delay = self.config["clear_traffic_delay"]
            self.cost = self.config["clear_traffic_cost"]

    def decay_condition(self):
        if self.condition == "H":
            if self.traffic_decay_count == 0:
                self.condition = "M"
                self.traffic_decay_count = self.config["medium_traffic_decay_count"]
                self.cost = self.config["medium_traffic_cost"]
                self.delay = self.config["medium_traffic_delay"]
                return True
            else:
                self.traffic_decay_count -= 1
                return False
        elif self.condition == "M":
            if self.traffic_decay_count == 0:
                self.condition = "C"
                self.cost = self.config["clear_traffic_cost"]
                self.delay = self.config["clear_traffic_delay"]
                return True
            else:
                self.traffic_decay_count -= 1
                return False

    def emergency_traffic_update(self):
        self.condition = "H"
        self.traffic_decay_count = self.config["heavy_traffic_decay_count"]
        self.cost = self.config["heavy_traffic_cost"]
        self.delay = self.config["heavy_traffic_delay"]


class Hospital:
    def __init__(self, location):
        self.location = location
        self.num_deployed = 0
        self.ambulances = []
        self.num_ambs = 2
        for i in range(self.num_ambs):
            self.ambulances.append(Ambulance(self.location, [], "idle"))
        self.max_roam = 1
        self.cost = 1
        self.delay = 1
        self.structure = "hospital"

    def num_active_duty(self):
        num = 0
        for ambulance in self.ambulances:
            if ambulance.state == "transport" or ambulance.state == "en_route":
                num += 1
        return num


class Building:
    def __init__(self):
        self.cost = 1
        self.delay = 1
        self.structure = "building"
