import heapq


def find_path(location, goal, layout, move_list, size):
    cost = get_cost(location, layout)
    start = Node(location, None, cost, [0, 0])
    hqueue = []
    get_next_states(start, start.cost, move_list, hqueue, size, layout, goal)
    while True:
        current = heapq.heappop(hqueue)
        node = current[1]
        node_cost = node.cost + get_cost(node.state, layout)
        if is_goal(node.state, goal):
            return trace_path(node), node.cost
        get_next_states(node, node_cost, move_list, hqueue, size, layout, goal)


def is_goal(state, goal):
    return state == goal


def get_cost(state, layout):
    return (layout[state[0]][state[1]]).cost


def is_wall(state, size, layout):
    if state[0] >= size[0] or state[1] >= size[1]:
        return True
    elif state[0] < 0 or state[1] < 0:
        return True
    elif not layout[state[0]][state[1]].structure == "road":
        return True
    else:
        return False


def heuristic(state, goal):
    return abs(state[0] - goal[0]) + abs(state[1] - goal[1])


def trace_path(goal_node):
    current = goal_node
    path = []
    while current.prev is not None:
        path.append(current.move)
        current = current.prev
    path.reverse()
    return path


def get_next_states(current, cost, movelist, queue, size, layout, goal):
    for move in movelist:
        next_state = [current.state[0] + move[0], current.state[1] + move[1]]
        if current.prev is not None:
            if next_state == current.prev.state:
                continue
        if is_goal(next_state, goal) or not is_wall(next_state, size, layout):
            heapq.heappush(queue, (cost + heuristic(next_state, goal), Node(next_state, current, cost, move)))


class Node:
    def __init__(self, state, prev, cost, move):
        self.state = state
        self.prev = prev
        self.cost = cost
        self.move = move
